package zohocom;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class webelement {

	//Formcreation
	
	@FindBy(how=How.XPATH, using="(//*[@class='fs-btn2 fs-btn2--style_create fs-btn2--size_large'])[2]") WebElement createtemp;
	@FindBy(how=How.XPATH, using="//*[@class='FT_b8709 FT_ec648']") WebElement form;
	@FindBy(how=How.XPATH, using="//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']") WebElement next;
	@FindBy(how=How.XPATH, using="//*[@type='text' and @name='formURL']") WebElement formurl;
	@FindBy(how=How.XPATH, using="//*[@type='text' and @name='formName']") WebElement formname;
	@FindBy(how=How.XPATH, using="//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']") WebElement startwithtemp;
	@FindBy(how=How.XPATH, using="//*[@class='FT_e5e71']") WebElement List;
	@FindBy(how=How.XPATH, using="//*[@style='fill: rgb(55, 64, 70); stroke: rgb(172, 181, 191);']") WebElement theme;
	@FindBy(how=How.XPATH, using="//*[@class='phx-Button-content' and text()='Next Step']") WebElement nextstep;
    @FindBy(how=How.XPATH, using="//*[@class=' phx-Button phx-Button--medium phx-Button--link phx-ActionButton']") WebElement finish;
	@FindBy(how=How.XPATH, using="(//*[@draggable='true'])[1]") WebElement drag;
	@FindBy(how=How.XPATH, using="//*[@class='BPF_fedaf']") java.util.List<WebElement> elelist;
	@FindBy(how=How.XPATH, using="//*[@class='VFD_1e67f']") WebElement live;
	@FindBy(how=How.XPATH, using="(//*[@draggable='true'])[12]") WebElement date;
	@FindBy(how=How.XPATH, using="(//*[@draggable='true'])[13]") WebElement fileupload;
	@FindBy(how=How.XPATH, using="(//*[@draggable='true'])[14]") WebElement matrix;
	
	//livepage
	
	@FindBy(how=How.XPATH, using="(//*[@class='fsRow fsFieldRow fsLastRow'])") java.util.List<WebElement> local;
	@FindBy(how=How.XPATH, using="(//*[@class='fsOptionLabel vertical'])[2]") WebElement sg;
	@FindBy(how=How.XPATH, using="(//*[@class='fsOptionLabel vertical'])[10]") WebElement staff;
	@FindBy(how=How.XPATH, using="//*[@class='fsField fsFormatText fsRequired   ']") WebElement textbox;
	@FindBy(how=How.XPATH, using="(//*[@class='fsField fsRequired vertical'])[14]") WebElement checkbox;
	@FindBy(how=How.XPATH, using="(//*[@class='fsField fsRequired '])[1]") WebElement textbox1;
	@FindBy(how=How.XPATH, using="(//*[@class='fsField fsRequired '])[2]") WebElement textbox2;
	@FindBy(how=How.XPATH, using="(//*[@class='fsField'])[5]") WebElement matrixclick;
	@FindBy(how=How.XPATH, using="(//*[@class='fsFileUploadButton'])[1]") WebElement uploadfile;
	
	
	//loginpage
	
	@FindBy(how=How.XPATH, using="(//*[@class='fi fi-angle-down show-for-medium'])[5]") WebElement logindropdown;
	@FindBy(how=How.XPATH, using="//*[@id='email']") WebElement emailid;
	@FindBy(how=How.XPATH, using="//*[@id='password']") WebElement password;
	@FindBy(how=How.XPATH, using="//*[@class='btn btn--login']") WebElement submit;
	@FindBy(how=How.XPATH, using="(//div//ul//a[@href='https://www.formstack.com/admin/'])[2]") WebElement login;
	


}
